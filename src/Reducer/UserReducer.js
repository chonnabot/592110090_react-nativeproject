export default (state = {}, action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            console.log('login action:', action)
            return {
                email: action.user.email,
                firstName: action.user.firstName,
                lastName: action.user.lastName,
                token: action.user.token
            }
        case 'USER_EDIT':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
            }

        default:
            return state
    }
}
