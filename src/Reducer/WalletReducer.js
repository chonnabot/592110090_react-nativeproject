export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_WALLET':
            const wallets = [
                ...state,
                 action.payload
            ]
            return wallets
        case 'DELETE_WALLET':
            const deleted = state.splice(state.indexOf(item => { return action.payload._id === item._id }), 1)
            return deleted
        case 'UPDATE_WALLET':
            const updateWallet = state.map(ele => {
                return ele._id === action.payload._id ? action.payload : ele
            })
            return updateWallet
        default:
            return state

    }
}
export const selectWalletReducer = (state = {}, action) => {
    switch (action.type) {
        case 'CLICK_WALLET':
            return action.payload
        default:
            return state
    }
}


