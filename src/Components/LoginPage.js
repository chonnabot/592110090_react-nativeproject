import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, InputItem, WhiteSpace,ActivityIndicator } from '@ant-design/react-native'

import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios'

import bgImage from '../Image/background1.jpg'
import logoImage from '../Image/logo.png'


class LoginPage extends React.Component {
    state = {
        email: '',
        password: '',
        isLoading: false
    }
    login = async () => {
        try {
            this.setState({ isLoading: true })
            const res = await axios.post('https://zenon.onthewifi.com/moneyGo/users/login', {
                email: this.state.email,
                password: this.state.password
            })
            const user = res.data.user
            await this.props.userLogin(user)
            this.setState({ isLoading: false })
            await this.props.history.push('/MainPage')
        } catch (error) {
            this.setState({ isLoading: false })
            const err = error.response.data.errors ? error.response.data.errors : error
            console.log('login response', error.response.data.errors);
            let errorMessage = ''
            if (err.email) errorMessage += 'Email ไม่ถูกต้อง' + '\n'
            if (err.password) errorMessage += 'Password ไม่ถูกต้อง' + '\n'
            Alert.alert('ข้อมูลไม่ถูกต้อง', errorMessage)
        }
    }
    goToRegisterPage = () => {
        return this.props.history.push('/RegisterPage')
    }
    // goToMainPage = () => {
    //     return this.props.history.push('/MainPage')
    // }

    render() {
        return (
            <ImageBackground source={bgImage} style={styles.imageBackgroundContainer}>
                <View style={styles.Background}>
                    <View style={styles.container}>
                        <View style={styles.logoBackground}>
                            <Image source={logoImage} style={styles.logo} />
                        </View>
                        <Text style={styles.logoText}>Money History</Text>
                    </View >

                    <View style={styles.containerBody}>
                        <InputItem
                            style={styles.inputBox}
                            placeholder={'Username'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.email}
                            onChange={value => this.setState({ email: value })}
                        />
                        <Icon
                            style={styles.icon}
                            name='user'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem style={styles.inputBox}
                            placeholder={'Password'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.password}
                            onChange={value => { this.setState({ password: value }) }}
                        />
                        <Icon
                            style={styles.icon}
                            name='lock'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <View style={styles.containerBody}>
                        <TouchableOpacity onPress={this.goToRegisterPage}>
                            <Text style={styles.text} onPress={this.goToRegisterPage}>ลงทะเบียน</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerBody}>
                        <Button style={styles.button} type="primary" onPress={this.login} > Login</Button>
                    </View>
                </View>
                {this.state.isLoading ?
                    <View 
                        style={{
                             width: '100%', 
                             height: '100%', 
                             alignItems: 'center', 
                             justifyContent: 'center', 
                             position: 'absolute', 
                             backgroundColor: 'rgba(255,255,255,0.7)' }}>
                        <ActivityIndicator size='large' color='rgba(16,142,233,10.5)'></ActivityIndicator>
                    </View> : <View></View>}

            </ImageBackground>



        );
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        userLogin: (user) => {
            dispatch({
                type: 'USER_LOGIN',
                user: user
            })
        }
    }
}

export default connect(null, mapDispatchToProps)(LoginPage)


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    Background: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 12,
        flex: 1
    },
    containerBody: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    imageBackgroundContainer: {
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    logoBackground: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        width: 200,
        height: 200,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        width: 150,
        height: 150
    },
    logoText: {
        // color: 'white',
        fontSize: 25,
        fontWeight: '500',
        marginTop: 10,
        marginBottom: 45
    },
    inputBox: {
        width: 300,
        height: 60,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 10

    },
    icon: {
        position: 'absolute',
        top: 1,
        left: 35
    },
    button: {
        borderRadius: 45,
        width: 150,
        height: 50,

    },
    text: {
        marginTop: 20,
        // color: 'white',
        fontWeight: '500',
        fontSize: 20,
        marginBottom: 10
    }

});







