import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, Input } from '@ant-design/react-native'

import { connect } from 'react-redux'

import wallet from '../Image/wallet.png'

class EditWalletPage extends React.Component {
    goToMainPage = () => {
        return this.props.history.push('/MainPage')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={this.goToMainPage}
                    />
                    <Text style={styles.textheader}>แก้ไขชื่อกระเป๋า</Text>
                </View>
                <View style={styles.body}>
                    <ImageBackground source={wallet} style={styles.logo} />
                    <TextInput style={styles.inputBox}
                        placeholder={'ชื่อกระเป๋า'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}>
                    </TextInput>
                    <Icon
                        style={styles.icon1}
                        name='smile'
                        size={25}
                        color='rgba(255,255,255,0.7)'
                    />
                    <Button style={styles.button} type="primary" onPress={this.goToMainPage} > ยืนยัน</Button>
                </View>
            </View >

        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    headerbody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',

    },
    logo: {
        top: -20,
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',

        alignItems: 'center',
        flex: 1
    },
    button: {
        marginTop: 20,
        borderRadius: 45,
        width: 150,
        height: 50,
        top: -40
    },
    inputBox: {
        width: 300,
        height: 65,
        borderRadius: 45,
        fontSize: 20,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginHorizontal: 25,
        marginBottom: 10,
        top: -60

    },
    icon1: {
        position: 'absolute',
        top: 160,
        left: 50
    },
    icon2: {
        position: 'absolute',
        top: 234,
        left: 50
    },
});



export default EditWalletPage



