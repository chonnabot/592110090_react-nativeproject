import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, Input, InputItem, WhiteSpace } from '@ant-design/react-native'
import { connect } from 'react-redux'

import wallet from '../Image/wallet.png'

class CreateWalletPage extends React.Component {
    state = {
        walletname: '',
        walletmoney: ''
    }

    goToMainPage = () => {
        return this.props.history.push('/MainPage')
    }

    saveToMainPage = () => {
        let newId = 0
        if (this.props.wallets.length > 0)
            newId = parseInt(this.props.wallets[this.props.wallets.length - 1]._id, 10) + 1
        const wallet = {
            _id: newId.toString(),
            name: this.state.walletname === '' ? 'Wallet Name' : this.state.walletname,
            balance: this.state.walletmoney === '' ? 0.00 : parseFloat(this.state.walletmoney),
            transactions: []
        }
        this.props.addWallet(wallet)
        Alert.alert('เพิ่มกระเป๋าใหม่', 'สำเร็จ')
        return this.props.history.push('/MainPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={() => this.props.history.goBack()}
                    />
                    <Text style={styles.textheader}>สร้างกระเป๋า</Text>
                </View>
                <View style={styles.body}>
                    <ImageBackground source={wallet} style={styles.logo} />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem
                            style={styles.inputBox}
                            placeholder={'ชื่อกระเป๋า'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.walletname}
                            onChange={value => { this.setState({ walletname: value }) }}
                        />
                        <Icon
                            style={styles.icon1}
                            name='smile'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />

                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem
                            style={styles.inputBox}
                            type="number"
                            placeholder={'จำนวนเงินเริ่มต้น'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.walletmoney}
                            onChange={value => { this.setState({ walletmoney: value }) }}
                        />
                        <Icon
                            style={styles.icon1}
                            name='credit-card'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />

                    </View>


                    <Button style={styles.button} type="primary" onPress={this.saveToMainPage} > ยืนยัน</Button>
                </View>
            </View >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addWallet: (wallet) => {
            dispatch({
                type: 'ADD_WALLET',
                payload: wallet
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CreateWalletPage)

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    containerBody: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    headerbody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',

    },
    logo: {
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',
        alignItems: 'center',
        flex: 1
    },
    button: {
        marginTop: 20,
        borderRadius: 45,
        width: 150,
        height: 50,

    },
    inputBox: {
        width: 300,
        height: 60,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 10,
        // top: -60

    },
    icon1: {
        position: 'absolute',
        top: 1,
        left: 30
    },
    icon2: {
        position: 'absolute',
        top: 234,
        left: 50
    },
});







