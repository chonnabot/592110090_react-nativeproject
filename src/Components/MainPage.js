import React from 'react';
import { StyleSheet, Image, ImageBackground, View, Text, Alert, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Icon, Button, List, Card, SwipeAction } from '@ant-design/react-native'
import { push, replace } from 'connected-react-router'

import { connect } from 'react-redux'

import pig from '../Image/pig.png'
import wallet from '../Image/wallet.png'

const Item = List.Item;

class MainPage extends React.Component {

    state = {

        totalBalance: '',
        wallets: [],
    }

    UNSAFE_componentWillMount() {
        this.setState({ wallets: this.props.wallets })
    }

    componentDidMount = () => {
        this.getTotalBalance()
    }

    getTotalBalance = () => {
        let total = 0
        this.props.wallets.forEach(ele => {
            total += ele.balance
        })
        this.setState({ totalBalance: total })
        return total
    }

    goToProfile = () => {
        return this.props.history.push('/ProfilePage')
    }
    goToEditWalletPage = () => {
        return this.props.history.push('/EditWalletPage')
    }

    goToLoginPage = () => {
        return this.props.history.push('/LoginPage')
    }

    goToWalletPage = (wallet) => {
        this.props.goToWalletPage(wallet)
        this.props.replace('/WalletPage')
    }

    goToCreateWalletPage = () => {
        return this.props.history.push('/CreateWalletPage')
    }


    render() {
        const right = [
            {
                text: 'Edit',
                onPress: () => { this.goToEditWalletPage() },
                style: { backgroundColor: 'orange', color: 'white' },
            },
            {
                text: 'Delete',
                onPress: () => console.log('delete'),
                style: { backgroundColor: 'red', color: 'white' },
            }]
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity style={styles.iconheaderleft}>
                        <Icon
                            name='arrow-left'
                            size={30}
                            color='white'
                            onPress={this.goToLoginPage}
                        />
                    </TouchableOpacity>
                    <Text style={styles.textheader}>Home</Text>
                    <TouchableOpacity style={styles.iconheaderright}>
                        <Icon
                            name='user'
                            size={30}
                            color='white'
                            onPress={this.goToProfile}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.headerbody}>

                    <View style={styles.headerbody2}>
                        <ImageBackground source={pig} style={styles.logo} />
                        <View style={styles.inputBox}>
                            <Text style={styles.text}>{parseFloat(this.state.totalBalance).toFixed(2)} THB</Text>
                        </View>
                    </View>

                    <View style={styles.headerbody2}>
                        <ScrollView>
                            <FlatList
                                data={this.state.wallets}
                                extraData={this.state}
                                style={{ width: '100%' }}
                                keyExtractor={(item) => item._id}
                                renderItem={({ item }) => (
                                    <SwipeAction
                                        autoClose
                                        style={{ backgroundColor: 'transparent' }}
                                        right={right}
                                        onOpen={() => console.log('open')}
                                        onClose={() => console.log('close')}
                                    >
                                        <Item
                                            extra={Number.parseFloat(item.balance).toFixed(2)}
                                            style={styles.Item}
                                            arrow="horizontal"
                                            onPress={() => this.goToWalletPage(item)}>

                                            <View style={{ flexDirection: 'row' }}>
                                                <Image source={wallet} style={{ width: 45, height: 45, right: 10 }} />
                                                <Text style={{ fontSize: 20, top: 10 }}>{item.name}</Text>
                                                {/* <Text style={{ fontSize: 24 }}>{Number.parseFloat(item.balance).toFixed(2)}</Text> */}
                                            </View>
                                        </Item>
                                    </SwipeAction>
                                )}
                            />
                        </ScrollView>
                    </View>
                </View>

                <View style={styles.footer}>
                    <View style={styles.boxfooter}>
                        <TouchableOpacity>
                            <Icon
                                name='plus'
                                size={30}
                                color='white'
                                onPress={this.goToCreateWalletPage}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.textfooter} onPress={this.goToCreateWalletPage}>เพิ่มกระเป๋าเงิน</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user,
        wallets: state.wallets
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        goToWalletPage: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        },
        replace: (path) => {
            dispatch(replace(path))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(MainPage)

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    iconheaderright: {
        position: 'absolute',
        top: 25,
        right: 30
    },
    iconfooter: {

    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    headerbody: {
        backgroundColor: '#666',
        alignItems: 'center',
        flex: 1.5
    },
    headerbody2: {
        backgroundColor: '#666',
        alignItems: 'center',
        flex: 1
    },
    viewScrollView: {
        backgroundColor: 'red',
        flex: 1

    },
    ScrollView: {
        marginHorizontal: 10,
        width: 200,
        top: -20,

    },
    Item: {
        height: 60,
        width: 360,
        justifyContent: 'center'
    },
    logo: {
        top: -20,
        width: 200,
        height: 200,
    },
    text: {
        marginTop: 20,
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10
    },
    bodybody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    footer: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
    },
    boxfooter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textfooter: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100,
        width: 70,
        height: 70,
        top: 100,
    },
    inputBox: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 80,
        borderRadius: 45,
        fontSize: 46,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        top: -50
    },
    wallet: {
        height: 80, backgroundColor: '#f4f4f4', paddingHorizontal: 24, margin: 8, borderRadius: 6, justifyContent: 'space-between', alignItems: 'center',
        flexDirection: 'row'
    }

});







