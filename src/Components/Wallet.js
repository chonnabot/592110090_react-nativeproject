import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView, FlatList } from 'react-native';
import { Icon, Button, Input, List, Card, } from '@ant-design/react-native'

import { connect } from 'react-redux'

import wallet from '../Image/wallet2.png'
import calendar from '../Image/calendar.png'

const Item = List.Item;

class WalletPage extends React.Component {

    state = {
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }


    UNSAFE_componentWillMount = () => {
        console.log('wallet props', this.props.selectwallet);
        let selected = this.props.wallets.find(ele => { return ele._id === this.props.selectwallet._id })
        this.setState({ wallet: selected })
    }


    componentDidMount = () => {
        console.log('state after mount', this.state);
    }

    getIncomePerDay = (list) => {
        let income = 0;
        list.forEach(ele => {
            if (ele.type === 'รายรับ') income += ele.money
        })
        return income
    }

    getExpensePerDay = (list) => {
        let expense = 0;
        list.forEach(ele => {
            if (ele.type === 'รายจ่าย') expense += ele.money
        })
        return expense
    }


    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'วันนี้'
        return dateToString(date)
    }



    goToMainPage = () => {
        return this.props.history.push('/MainPage')
    }

    goToRevenuePage = () => {
        return this.props.history.push('/RevenuePage')
    }
    goToExpensesPage = () => {
        return this.props.history.push('/ExpensesPage')
    }


    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={this.goToMainPage}
                    />
                    <Text style={styles.textheader}>{this.state.wallet.name}</Text>
                </View>

                <View style={styles.bodybody}>

                    <Text style={styles.text}>จำนวนเงินในกระเป๋า</Text>
                    <View style={{ width: '60%', height: 1, backgroundColor: 'rgba(255,255,255,0.5)', margin: 6 }} />
                    <Text style={styles.text}>{parseFloat(this.state.wallet.balance).toFixed(2)}</Text>

                    <ScrollView>
                        <FlatList
                            data={this.state.wallet.transactions.reverse()}
                            extraData={this.state}
                            keyExtractor={(item, index) => item.key}
                            renderItem={({ item }) => (
                                <List>
                                    <Item style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#708090' }} >
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 18 }}></Text>
                                            <Text style={{
                                                fontSize: 18,
                                                color: 'white',
                                                fontWeight: 'bold',
                                            }}>{this.formatDate(item.date)}</Text>
                                            <Text style={{ fontSize: 18 }}></Text>
                                        </View>
                                    </Item>
                                    <Item style={{ flexDirection: 'row', alignItems: 'center' }}  >
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 14 }}></Text>
                                            <Text style={{ fontSize: 14 }}>จำนวนเงินคงเหลือ : {parseFloat(this.getIncomePerDay(item.list) - this.getExpensePerDay(item.list)).toFixed(2)}</Text>
                                            <Text style={{ fontSize: 14 }}></Text>
                                        </View>
                                    </Item>
                                    <Item style={{ flexDirection: 'row', alignItems: 'center' }} >
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <Text style={{ fontSize: 14 }}></Text>
                                            <Text style={{ fontSize: 14 }}>รายรับ: {parseFloat(this.getIncomePerDay(item.list)).toFixed(2)}    รายจ่าย: {parseFloat(this.getExpensePerDay(item.list)).toFixed(2)}</Text>
                                            <Text style={{ fontSize: 14 }}></Text>
                                        </View>
                                    </Item>

                                    <Item style={styles.Item} >
                                        {item.list.map((ele) => {
                                            return (
                                                <Item style={{ flexDirection: 'row', }} >
                                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                        <Text style={{ fontSize: 16 }}>{ele.type}</Text>
                                                        <Text style={{ fontSize: 16 }}>{ele.catagory}</Text>
                                                        <Text style={{ fontSize: 16 }}>{parseFloat(ele.money).toFixed(2)}</Text>
                                                    </View>

                                                </Item>
                                            )
                                        })}
                                    </Item>
                                </List>
                            )}
                        />
                    </ScrollView>

                </View>


                <View style={styles.footer}>

                    <View style={styles.boxfooter}>
                        <TouchableOpacity>
                            <Icon
                                name='plus'
                                size={28}
                                color='white'
                                onPress={this.goToRevenuePage}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.textfooter}
                                onPress={this.goToRevenuePage}>เพิ่มรายรับ</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.boxfooter}>
                        <TouchableOpacity>
                            <Icon
                                name='plus'
                                size={28}
                                color='white'
                                onPress={this.goToExpensesPage}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Text style={styles.textfooter}
                                onPress={this.goToExpensesPage}>เพิ่มรายจ่าย</Text>
                        </TouchableOpacity>


                    </View>

                </View>

            </View >

        );
    }
}

const mapStateToProps = (state) => {
    return {
        selectwallet: state.selectwallet,
        wallets: state.wallets
    }
}

export default connect(mapStateToProps)(WalletPage)

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 18,
        left: 30
    },
    text: {
        marginTop: 10,
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    logo: {
        width: 200,
        height: 200,
        justifyContent: 'center',
        alignItems: 'center',
        top: -15
    },
    headerbody2: {
        backgroundColor: '#666',
        alignItems: 'center',
        flex: 1
    },
    Item: {
        width: 360,
        flexDirection: 'row',
    },
    bodybody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    iconheader: {
        position: 'absolute',
        top: 30,
        left: 30
    },
    inputBox: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 80,
        borderRadius: 45,
        fontSize: 46,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
    },
    footer: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
    },
    boxfooter: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textfooter: {
        marginTop: 20,
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 12
    },
});





