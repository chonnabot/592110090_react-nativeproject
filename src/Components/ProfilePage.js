import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, ScrollView } from 'react-native';
import { Icon, Button, Input, WhiteSpace, ActivityIndicator } from '@ant-design/react-native'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import { connect } from 'react-redux'

import profile from '../Image/profile.png'

class ProfilePage extends React.Component {

    state = {
        imagePath: '',
        isLoading: false
    }
  

    componentDidMount() {
        this.getImage()
    }

    goToLoginPage = () => {
        return this.props.history.push('/LoginPage')
    }

    getImage = () => {
        const { user } = this.props
        this.setState({ isLoading: true })
        axios.get('https://zenon.onthewifi.com/moneyGo/users', {
            headers: {
                Authorization: `Bearer ${this.props.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => {
                this.setState({ isLoading: false })
                console.log(err)
            })
            .finally(() => { console.log('Finally') })
    }


    goToMainPage = () => {
        return this.props.history.push('/MainPage')
    }
    goToEditProfilePage = () => {
        return this.props.history.push('/EditProfilePage')
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={this.goToMainPage}
                    />
                    <Text style={styles.textheader}>ข้อมูลส่วนตัว</Text>
                    <Icon
                        style={styles.iconheaderright}
                        name='edit'
                        size={30}
                        color='white'
                        onPress={this.goToEditProfilePage}
                    />
                </View>
                <View style={styles.body}>

                    
                        <ImageBackground source={profile} style={styles.logo}>
                            <Image
                                style={styles.logo2}
                                source={{ uri: this.state.imagePath }}
                            />
                        </ImageBackground>
                    
                        {this.state.isLoading ?
                            <View
                                style={{
                                    top: 30,
                                    width: 150,
                                    height: 150,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    backgroundColor: 'rgba(255,255,255,0.7)'
                                }}>
                                <ActivityIndicator size='large' color='rgba(16,142,233,10.5)'></ActivityIndicator>
                            </View> : <View></View>}

                  
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <View style={styles.inputBox}>
                        <Text style={styles.text}>{this.props.user.firstName}</Text>
                        <Icon
                            style={styles.icon1}
                            name='solution'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />

                    </View>
                    <View style={styles.inputBox}>
                        <Text style={styles.text}>{this.props.user.lastName}</Text>
                        <Icon
                            style={styles.icon1}
                            name='solution'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <Button style={styles.button} type="primary" onPress={this.goToLoginPage} > Long out</Button>
                </View>
            </View >

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(ProfilePage)

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    iconheaderright: {
        position: 'absolute',
        top: 20,
        right: 30
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    headerbody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',

    },
    logo: {
        top: 30,
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo2: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',

        alignItems: 'center',
        flex: 1
    },
    text: {
        top: 20,
        fontSize: 20,
        color: 'white',
    },
    textBox: {
        top: 50,
        width: 300,
        height: 65,
        borderRadius: 45,
        fontSize: 20,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'white',
        marginHorizontal: 25,
        marginBottom: 10,
    },
    inputBox: {
        width: 300,
        height: 60,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 10,
        // top: -60

    },
    icon1: {
        position: 'absolute',
        top: 20,
        left: 20
    },
    button: {
        marginTop: 20,
        borderRadius: 45,
        width: 150,
        height: 50,
        top: 40
    },
});




