import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, InputItem, WhiteSpace } from '@ant-design/react-native'

import { connect } from 'react-redux'

import expenses from '../Image/expenses.png'

class ExpensesPage extends React.Component {
    state = {
        catagory: '',
        description: '',
        money: '',
    }

    onChangeTypeSelect = (index, value) => {
        this.setState({ catagory: value })
    }

    formatDate = (date) => {
        function dateToString(date) {
            let d = date,
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
        }
        const today = new Date()
        if (dateToString(today) === dateToString(date))
            return 'Today'
        return dateToString(date)
    }

    addNewTransaction = () => {
        let updateWallet = {}
        const todayDate = new Date()
        const newTransaction = {
            type: 'รายจ่าย',
            description: this.state.description,
            catagory: this.state.catagory,
            money: parseInt(this.state.money, 10)
        }
        if (this.props.selectwallet.transactions[0] && this.props.selectwallet.transactions[0].date) {
            if (this.formatDate(this.props.selectwallet.transactions[0].date) === this.formatDate(todayDate)) {
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance - newTransaction.money,
                    transactions: this.props.selectwallet.transactions
                }
                updateWallet.transactions[0].list.push(newTransaction)
            } else {
                const newTransactions = [{
                    date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                    list: [newTransaction]
                }, ...this.props.selectwallet.transactions]
                updateWallet = {
                    ...this.props.selectwallet,
                    balance: this.props.selectwallet.balance - newTransaction.money,
                    transactions: newTransactions
                }
            }
        } else {
            const newTransactions = [{
                date: new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate()),
                list: [newTransaction]
            }, ...this.props.selectwallet.transactions]
            updateWallet = {
                ...this.props.selectwallet,
                balance: this.props.selectwallet.balance - newTransaction.money,
                transactions: newTransactions
            }
        }
        this.props.updateWallet(updateWallet)
        this.props.clickWallet(updateWallet)
        this.props.history.replace('/WalletPage')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={() => this.props.history.replace('/WalletPage')}
                    />
                    <Text style={styles.textheader}>รายจ่าย</Text>
                </View>
                <View style={styles.bodybody}>
                    <View style={styles.body}>
                        <ImageBackground source={expenses} style={styles.logo} />
                        <InputItem
                            style={styles.inputBox}
                            placeholder={'กรอกจำนวนเงิน'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            type="number"
                            value={this.state.money}
                            onChange={value => { this.setState({ money: value }) }}

                        />
                        <WhiteSpace />
                        <WhiteSpace />
                        <WhiteSpace />
                        <InputItem
                            style={styles.inputBox}
                            placeholder={'หัวข้อรายจ่ายรายจ่าย'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.catagory}
                            onChange={value => { this.setState({ catagory: value }) }}

                        />
                        {/* <View style={styles.inputBox2}>
                            <TouchableOpacity onPress={this.goToAddTopicExpensesPage} >
                                <Text style={styles.text4} >เลือกหัวข้อรายจ่ายรายจ่าย
                                </Text>
                                <Icon
                                    style={styles.iconbody}
                                    name='right'
                                    size={18}
                                    color='white'
                                    onPress={this.goToAddTopicRevenPage}
                                />
                            </TouchableOpacity>

                        </View> */}

                        <Button style={styles.button} type="primary" onPress={this.addNewTransaction} > ยืนยัน</Button>
                    </View>
                </View>

            </View >

        );
    }
}

const mapStateToProps = (state) => {
    return {
        wallets: state.wallets,
        selectwallet: state.selectwallet
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateWallet: (newWallet) => {
            dispatch({
                type: 'UPDATE_WALLET',
                payload: newWallet
            })
        },
        clickWallet: (wallet) => {
            dispatch({
                type: 'CLICK_WALLET',
                payload: wallet
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExpensesPage)
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    text1: {
        marginTop: 20,
        color: 'white',
        fontSize: 23,
        fontWeight: 'bold',
        marginBottom: 10,
        top: -100,
        left: -70
    },
    text2: {
        marginTop: 20,
        color: 'white',
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    text3: {
        marginTop: 20,
        color: 'white',
        fontSize: 23,
        fontWeight: 'bold',
        marginBottom: 10,
        top: -140,
        left: -60
    },
    text4: {
        marginTop: 20,
        color: 'white',
        fontSize: 22,
        marginBottom: 20,

    },
    logo: {
        width: 250,
        height: 250,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bodybody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    body: {
        backgroundColor: '#666',
        alignItems: 'center',
        flex: 1
    },
    inputBox: {
        width: 300,
        height: 60,
        borderRadius: 10,
        fontSize: 24,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 20,
        // top: -60

    },
    inputBox1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 300,
        height: 100,
        borderRadius: 45,
        fontSize: 46,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        top: -110,
        marginBottom: 20,
        flexDirection: 'row',
    },
    inputBox2: {
        paddingLeft: 60,
        width: 333,
        height: 60,
        borderRadius: 10,
        fontSize: 46,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        top: 10,
        marginBottom: 10
    },
    iconbody: {
        position: 'absolute',
        top: 25,
        left: 230
    },
    button: {
        top: 25,
        marginTop: 20,
        borderRadius: 45,
        width: 150,
        height: 50,
    },
});







