import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, InputItem, WhiteSpace, ActivityIndicator } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios'
import { connect } from 'react-redux'

import profile from '../Image/profile.png'

class EditProfliePage extends React.Component {

    state = {
        firstName: this.props.user.firstName,
        lastName: this.props.user.lastName,
        imagePath: '',
        isLoading: false
    }

    goToMainPage = () => {
        return this.props.history.push('/ProfilePage', {
            item: 'profile'
        })
    }

    selectImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.uri) {
                const formData = new FormData()

                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                this.setState({ isLoading: true })
                axios.post('https://zenon.onthewifi.com/moneyGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${this.props.user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            isLoading: false,
                        })
                    })
                    .catch(error => {
                        this.setState({ isLoading: false })
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }

    componentDidMount() {
        this.getImage()
    }

    getImage = () => {
        const { user } = this.props
        this.setState({ isLoading: true })
        axios.get('https://zenon.onthewifi.com/moneyGo/users', {
            headers: {
                Authorization: `Bearer ${this.props.user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    isLoading: false
                })


            })
            .catch(err => {
                this.setState({ isLoading: false })
                console.log(err)
            })
            .finally(() => { console.log('Finally') })
    }

    onSave = () => {
        axios({
            url: 'https://zenon.onthewifi.com/moneyGo/users',
            method: 'put',
            headers: {
                'Authorization': `Bearer ${this.props.user.token}`
            },
            data: {
                firstName: this.state.firstName,
                lastName: this.state.lastName
            }
        }).then(() => {
            return axios.get('https://zenon.onthewifi.com/moneyGo/users', {
                headers: {
                    'Authorization': `Bearer ${this.props.user.token}`
                },
            }
            )
        })
            .then(response => {
                this.props.saveUserInRedux(response.data.user);
                this.props.history.push('/ProfilePage', {
                    item: 'profile'
                })
            }).catch(e => {
                console.log("error " + e)
            })

    }

    render() {
        const { user, editUser } = this.props
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Icon
                        style={styles.iconheaderleft}
                        name='arrow-left'
                        size={30}
                        color='white'
                        onPress={this.goToMainPage}
                    />
                    <Text style={styles.textheader}>แก้ไขข้อมูลส่วนตัว</Text>
                </View>
                <View style={styles.body}>
                    <TouchableOpacity onPress={this.selectImage} style={[styles.img]}>

                        <ImageBackground source={profile} style={styles.logo}>
                            <Image
                                style={styles.logo2}
                                source={{ uri: this.state.imagePath }}
                            />
                        </ImageBackground>

                        <Icon
                            style={{ left: 118, }}
                            raised
                            size={30}
                            name='camera'
                            type='font-awesome'
                            color='white'
                        />
                        {this.state.isLoading ?
                            <View
                                style={{
                                    top: 30,
                                    width: 150,
                                    height: 150,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    backgroundColor: 'rgba(255,255,255,0.7)'
                                }}>
                                <ActivityIndicator size='large' color='rgba(16,142,233,10.5)'></ActivityIndicator>
                            </View> : <View></View>}

                    </TouchableOpacity>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />

                    <View style={styles.containerBody}>
                        <InputItem
                            size={20}
                            style={styles.inputBox}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            // placeholder={this.props.user.firstName}
                            value={this.state.firstName}
                            onChange={firstName => {
                                this.setState({
                                    firstName,
                                });
                            }}
                        />
                        <Icon
                            style={styles.icon1}
                            name='edit'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />

                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem
                            size={20}
                            style={styles.inputBox}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            // placeholder={this.props.user.firstName}
                            value={this.state.lastName}
                            onChange={lastName => {
                                this.setState({
                                    lastName,
                                });
                            }}
                        />
                        <Icon
                            style={styles.icon1}
                            name='edit'
                            size={25}
                            color='rgba(255,255,255,0.7)'
                        />

                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    

                    <Button style={styles.button} type="primary" onPress={() => this.onSave()} > ยืนยัน</Button>
                </View>
            </View >

        );
    }
}
const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        saveUserInRedux: ({ firstName, lastName }) => {
            dispatch({
                type: 'USER_EDIT',
                firstName: firstName,
                lastName: lastName
            })
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditProfliePage)
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },
    header: {
        backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconheaderleft: {
        position: 'absolute',
        top: 20,
        left: 30
    },
    iconheaderright: {
        position: 'absolute',
        top: 20,
        right: 30
    },
    textheader: {
        marginTop: 20,
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10
    },
    containerBody: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 330,
    },
    headerbody: {
        backgroundColor: '#666',
        justifyContent: 'center',
        alignItems: 'center',

    },
    logo: {
        top: 30,
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo2: {
        width: 150,
        height: 150,
        justifyContent: 'center',
        alignItems: 'center',
    },
    body: {
        backgroundColor: '#666',

        alignItems: 'center',
        flex: 1
    },
    button: {
        marginTop: 10,
        borderRadius: 45,
        width: 150,
        height: 50,
        top: 40
    },
    text: {
        top: 10,
        fontSize: 20,
        color: 'rgba(255,255,255,0.7)',
    },
    textBox: {
        top: 50,
        width: 300,
        height: 60,
        borderRadius: 45,
        fontSize: 20,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginHorizontal: 25,
        marginBottom: 10,


    },
    inputBox: {
        width: 300,
        height: 60,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 10,
        // top: -60

    },
    icon1: {
        position: 'absolute',
        top: 2,
        left: 35
    },
});







