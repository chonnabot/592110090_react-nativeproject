import React from 'react';
import { StyleSheet, ImageBackground, Image, View, Text, Alert, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { Icon, Button, InputItem, WhiteSpace } from '@ant-design/react-native'
import axios from 'axios'

import { connect } from 'react-redux'

import bgImage from '../Image/background1.jpg'

export class RegisterPage extends React.Component {
    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: ''
    }

    userSignup = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.firstname === '' || this.state.lastname === '') {
            Alert.alert('ข้อมูลไม่ถูกต้อง', 'กรุณากรอกข้อมูลให้ครบ')
        } else {
            try {
                await axios.post('https://zenon.onthewifi.com/moneyGo/users/register', {
                    email: this.state.email,
                    password: this.state.password,
                    firstName: this.state.firstname,
                    lastName: this.state.lastname
                })
                await Alert.alert('ลงทะเบียนเรียบร้อย')
                await this.props.history.replace('/LoginPage')
            } catch (error) {
                console.log('signup error', error.response.data.errors)
            }
        }
    }
    // goToLoginPage = () => {
    //     return this.props.history.push('/LoginPage')
    // }
    render() {
        return (
            <ImageBackground source={bgImage} style={styles.imageBackgroundContainer}>
                <View style={styles.Background}>
                    <Text style={styles.logoText}>ลงทะเบียน</Text>
                    <View style={styles.containerBody}>
                        <InputItem
                            style={styles.inputBox}
                            placeholder={'Email'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.email}
                            onChange={value => this.setState({ email: value })}
                        />
                        <Icon
                            style={styles.icon}
                            name='info-circle'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem style={styles.inputBox}
                            placeholder={'Password'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.password}
                            onChange={value => { this.setState({ password: value }) }}
                        />
                        <Icon
                            style={styles.icon}
                            name='info-circle'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem style={styles.inputBox}
                            placeholder={'ชื่อ'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.firstname}
                            onChange={value => { this.setState({ firstname: value }) }}
                        />
                        <Icon
                            style={styles.icon}
                            name='info-circle'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <InputItem style={styles.inputBox}
                            placeholder={'นามสกุล'}
                            placeholderTextColor={'rgba(255,255,255,0.7)'}
                            value={this.state.lastname}
                            onChange={value => { this.setState({ lastname: value }) }}
                        />
                        <Icon
                            style={styles.icon}
                            name='info-circle'
                            size={26}
                            color='rgba(255,255,255,0.7)'
                        />
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <Icon
                            style={styles.icon2}
                            name='arrow-left'
                            size={22}
                            color='rgba(0,0,0,0.35)'
                        />
                        <TouchableOpacity>
                            <Text style={styles.text} onPress={() => this.props.history.goBack()}>กลับ</Text>
                        </TouchableOpacity>
                    </View>
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <WhiteSpace />
                    <View style={styles.containerBody}>
                        <Button style={styles.button} type="primary" onPress={this.userSignup}> ยืนยัน</Button>

                    </View>
                </View>
            </ImageBackground >



        );
    }
}
const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',

    },
    Background: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 12,
        flex: 1
    },
    containerBody: {
        justifyContent: 'center',
        alignItems: 'center',
        // flex: 1
    },
    imageBackgroundContainer: {
        width: null,
        height: null,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    logoBackground: {
        backgroundColor: 'rgba(0,0,0,0.35)',
        width: 200,
        height: 200,
        borderRadius: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoText: {
        // color: 'white',
        fontSize: 25,
        fontWeight: '500',
        marginTop: 10,
        marginBottom: 45
    },
    inputBox: {
        width: 300,
        height: 65,
        borderRadius: 10,
        fontSize: 16,
        paddingLeft: 60,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'rgba(255,255,255,0.7)',
        marginBottom: 10

    },
    icon: {
        position: 'absolute',
        top: 4,
        left: 35
    },
    icon2: {
        position: 'absolute',
        top: 1,
        left: -30
    },
    button: {
        borderRadius: 45,
        width: 150,
        height: 50,
        top: -55,

    },
    text: {
        marginTop: 20,
        // color: 'white',
        fontWeight: '500',
        fontSize: 20,
        marginBottom: 10,
        top: -20,
    }

});


export default RegisterPage



