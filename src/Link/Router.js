import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { store, history } from './AppStore'


import LoginPage from '../Components/LoginPage'
import RegisterPage from '../Components/RegisterPage';
import MainPage from '../Components/MainPage';
import CreateWalletPage from '../Components/CreateWalletPage';
import WalletPage from '../Components/Wallet';
import RevenuePage from '../Components/RevenuePage';
import ExpensesPage from '../Components/ExpensesPage';
import ProfilePage from '../Components/ProfilePage';
import EditProfilePage from '../Components/EditProfilePage';
import EditWalletPage from '../Components/EditWalletPage';




class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route exact path="/LoginPage" component={LoginPage} />
                        <Route exact path="/RegisterPage" component={RegisterPage} />
                        <Route exact path="/MainPage" component={MainPage} />
                        <Route exact path="/CreateWalletPage" component={CreateWalletPage} />
                        <Route exact path="/WalletPage" component={WalletPage} />
                        <Route exact path="/RevenuePage" component={RevenuePage} />
                        <Route exact path="/ExpensesPage" component={ExpensesPage} />
                        <Route exact path="/ProfilePage" component={ProfilePage} />
                        <Route exact path="/EditProfilePage" component={EditProfilePage} />
                        <Route exact path="/EditWalletPage" component={EditWalletPage} />
                        <Redirect to="/LoginPage" />
                    </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}



export default Router
