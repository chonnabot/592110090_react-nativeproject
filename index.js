/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import LoginPage from './src/Components/LoginPage';
import RegisterPage from './src/Components/RegisterPage';
import MainPage from './src/Components/MainPage';
import CreateWalletPage from './src/Components/CreateWalletPage';
import WalletPage from './src/Components/Wallet';
import RevenuePage from './src/Components/RevenuePage';
import ExpensesPage from './src/Components/ExpensesPage';
import ProfilePage from './src/Components/ProfilePage';
import EditWalletPage from './src/Components/EditWalletPage';
import EditProfilePage from './src/Components/EditProfilePage';

import Router from './src/Link/Router';

import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Router);
